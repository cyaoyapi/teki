//
//  ViewController.swift
//  Teki
//
//  Created by Cyrille YAO on 11/11/19.
//  Copyright © 2019 cyaoio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var quoteLabel: UILabel!
    
    @IBAction func changeQuote() {
        // Celebrities
        let celebrities = ["le Steve Jobs", "le Zinedine Zidane", "la Madonna", "le Karl Lagarfeld", "la Scarlett Johansson"]
        let chosenCelebretieIndex = Int(arc4random_uniform(UInt32(celebrities.count)))
        let chosenCelebritie = celebrities[chosenCelebretieIndex]
        // Activities
        let activities = ["du dancefloor", "du barbecue", "de la surprise ratée", "des blagues lourdes", "de la raclette party"]
        let chosenActivitieIndex = Int(arc4random_uniform(UInt32(activities.count)))
        let chosenActivitie = activities[chosenActivitieIndex]
        quoteLabel.text = "Tu es " + chosenCelebritie + " " + chosenActivitie
        
    }
    
}

